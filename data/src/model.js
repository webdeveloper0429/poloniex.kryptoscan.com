import { MongoClient } from 'mongodb';
const url = "mongodb://localhost:27017/poloniexdb";

const delOldData = (db_from, days)=>{
	let now_time = Math.floor(Date.now()/1000);
	let query = { 
					time_stamp: { 
									$lt: now_time-(1440*days)
								}
				};
	MongoClient.connect(url, function(err, db) {
		if (err) throw err;
		db.collection(db_from).deleteMany(query, function(err, obj) {
			if (err) throw err;
			console.log(db_from+'-----'+obj.result.n + " document(s) deleted");
			db.close();
		});
	});
}

const delOldDataBundle = ()=>{ 
	delOldData('min_1', 1);
	delOldData('min_5', 2);
	delOldData('min_15', 3);
	delOldData('min_30', 4);
	delOldData('min_60', 5);
	delOldData('min_120', 6);
	delOldData('min_240', 7);
}
module.exports = {
	delOldDataBundle
}
		